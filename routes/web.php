<?php

/*
 * Skeleton routes
 * ------------------
 */
Route::resource('foo', 'FooController', ['only' => ['index', 'create', 'store', 'destroy'], 'as' => 'skeleton']);