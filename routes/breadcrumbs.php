<?php
/*
 * Skeleton breadcrumbs
 * ------------------
 */
Breadcrumbs::register('skeleton::foo', function ($breadcrumbs) {
    $breadcrumbs->parent('production');
    $breadcrumbs->push(__('skeleton::common.foo'), route('skeleton.foo.index'));
});

Breadcrumbs::register('skeleton::foo.create', function ($breadcrumbs) {
    $breadcrumbs->parent('skeleton::foo');
    $breadcrumbs->push(__('skeleton::common.create'), route('skeleton.foo.create'));
});