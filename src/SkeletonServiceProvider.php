<?php

namespace Geonodo\Skeleton;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class SkeletonServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishConfig();
        $this->registerRoutes();
        $this->registerResources();
        $this->registerTranslations();
        $this->loadMigrations();
        $this->publishAssets();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Setup the configuration.
     *
     * @return void
     */
    protected function publishConfig()
    {
        $this->publishes([
            __DIR__ . '/../config/skeleton.php' => config_path('skeleton.php'),
        ]);
    }

    /**
     * Register the resources.
     *
     * @return void
     */
    protected function registerResources()
    {
        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'skeleton');
    }

    /**
     * Register the routes and breadcrumbs.
     *
     * @return void
     */
    protected function registerRoutes()
    {
        Route::group([
            'prefix'     => config('skeleton.routes_prefix', 'skeleton'),
            'namespace'  => 'Geonodo\Skeleton\Http\Controllers',
            'middleware' => config('skeleton.middleware', ['web', 'auth']),
        ], function () {
            // Routes
            $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');

            // Breadcrumbs
            $this->loadRoutesFrom(__DIR__ . '/../routes/breadcrumbs.php');
        });
    }

    /**
     * Register the translations.
     *
     * @return void
     */
    protected function registerTranslations()
    {
        $this->loadTranslationsFrom(__DIR__ . '/../resources/lang', 'skeleton');
    }

    /**
     * Register the translations.
     *
     * @return void
     */
    protected function loadMigrations()
    {
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');
    }

    /**
     * Publish the assets
     */
    protected function publishAssets()
    {
        $this->publishes([
            __DIR__ . '/../resources/assets/js/components' => base_path('resources/assets/js/vendor/skeleton'),
        ], 'assets');
    }
}