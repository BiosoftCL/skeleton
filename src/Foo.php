<?php

namespace Geonodo\Skeleton;

use Illuminate\Database\Eloquent\Model;

class Foo extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'skeleton_foo';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['bar'];
}