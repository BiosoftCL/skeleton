<?php

namespace Geonodo\Skeleton\Http\Controllers;

use Geonodo\Skeleton\Foo;
use Geonodo\Skeleton\Http\Requests\StoreFoo;

class FooController
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $collection = Foo::paginate(15);

        return view('skeleton::foo.index', compact('collection'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('skeleton::foo.create');
    }

    /**
     * @param StoreFoo $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreFoo $request)
    {
        Foo::create([
            'bar' => $request->input('bar'),
        ]);

        return redirect()->route('skeleton.foo.index');
    }

    /**
     * @param Foo $foo
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Foo $foo)
    {
        try {
            $foo->delete();
            flash(__('skeleton::common.deleted'));
        } catch (\Exception $e) {
            logger('\Geonodo\Skeleton\Http\Controllers\FooController@destroy', ['foo' => $foo->id, 'msg' => $e->getMessage()]);

            flash(__('skeleton::common.deleted_error'), 'error');
        }

        return redirect()->route('skeleton.foo.index');
    }
}