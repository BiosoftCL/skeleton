<?php

return [
    'foo' => 'Foo',
    'bar' => 'Bar',

    'create' => 'Crear',
    'creating' => 'Creando',

    'delete' => 'Eliminar',
    'delete_confirm' => '¿Desea eliminar :foo?'
];