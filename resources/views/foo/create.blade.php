@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                {!! Breadcrumbs::render('skeleton::foo.create') !!}
            </div>
        </div>
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">{{ __('skeleton::common.create') }}</div>
                    <div class="panel-body">

                        {!! Form::open(['url' => route('skeleton.foo.index'), 'class' => 'form-horizontal']) !!}

                        <div class="form-group {{ $errors->has('bar') ? 'has-error' : ''}}">
                            {!! Form::label('bar', __('skeleton::common.bar'), ['class' => 'col-md-4 control-label required']) !!}
                            <div class="col-md-6">
                                {!! Form::text('bar', null, ['class' => 'form-control']) !!}
                                {!! $errors->first('bar', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-4">
                                {!! Form::submit(__('skeleton::common.create'), ['class' => 'btn btn-primary btn-submit-disable', 'data-loading-text' => __('skeleton::common.creating')]) !!}
                            </div>
                        </div>

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection