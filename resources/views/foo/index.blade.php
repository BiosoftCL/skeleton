@extends('layouts.app')

@section('content')
    <div class="container" id="app">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                {!! Breadcrumbs::render('skeleton::foo') !!}
            </div>
        </div>
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading clearfix">
                        <h4 class="panel-title panel-title-index col-sm-6">{{ __('skeleton::common.foo') }}</h4>
                        <hr class="visible-xs">
                        <form class="col-md-6 text-right form-inline">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon"><i class="glyphicon glyphicon-search"></i></div>
                                    <input type="text" class="form-control" id="searchTerm" name="search"
                                           data-table="external-connections" placeholder="{{__('form.filter')}}">
                                </div>
                            </div>
                            <a href="{{ route('skeleton.foo.create') }}" class="btn btn-default"
                               title="{{ __('skeleton::common.create') }}">{{ __('skeleton::common.create') }}</a>
                        </form>
                    </div>

                    <div class="panel-body">
                        @include('flash::message')
                        <table class="table table-borderless">
                            <thead>
                            <tr>
                                <th>{{ __('skeleton::common.bar') }}</th>
                                <th class="text-right">
                                    <pagination-header :data="{{ $collection->toJson() }}"></pagination-header>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($collection as $foo)
                                <tr>
                                    <td>{{ $foo->bar }}</td>
                                    <td class="text-right text-nowrap">
                                        {!! Form::open([
                                            'method'=>'DELETE',
                                            'url' => route('skeleton.foo.destroy', $foo->id),
                                            'style' => 'display:inline'
                                        ]) !!}
                                        {!! Form::button('<i class="glyphicon glyphicon-trash" aria-hidden="true"></i>', [
                                            'type' => 'submit',
                                            'class' => 'btn btn-sm btn-danger',
                                            'title' => __('skeleton::common.delete'),
                                            'onclick'=>'return confirm("'.__('skeleton::common.delete_confirm', ['foo' => $foo->bar]).'")'
                                        ]) !!}
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{ $collection->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection