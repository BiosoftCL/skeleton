<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Routes prefix
    |--------------------------------------------------------------------------
    |
    | Texto prefijo para las rutas del paquete
    |
    */

    'routes_prefix' => 'skeleton',

    /*
    |--------------------------------------------------------------------------
    | Middleware
    |--------------------------------------------------------------------------
    |
    | Middleware por los que filtrarán las rutas del paquete
    |
    */

    'middleware' => ['web', 'auth', 'role:editor', 'session.instance'],

];
